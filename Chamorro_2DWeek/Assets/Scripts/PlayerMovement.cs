﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
//using System.Runtime.Hosting;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerMovement : MonoBehaviour
{

    public TextMeshProUGUI countText; 
    public int count = 0;
    Rigidbody2D rB2D;
    public float runSpeed;//Run member
    public float jumpSpeed; //Jump member
    //public GameEnding gameEnding; //Calls the GameEnding script to player movement
    
    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        SetCountText();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            //Does a thin boxcast directly below to see if we are on the floor
            int levelMask = LayerMask.GetMask("Level");

            //Rectangle RayCast, the size of the player script and it checks if there is anything beneath it
            if(Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }


    void OnTriggerEnter2D(Collider2D Collectible) //When Santa falls below the platform, the scene will restart 
    {
        if (Collectible.gameObject.CompareTag("Collectible"))
        {
            Collectible.gameObject.SetActive(false);
            count++;
            SetCountText();            
        }
    }

    void OnColliderEnter2D(Collider2D End)
    {
        if (End.gameObject.CompareTag("End"))
        {
            End.gameObject.SetActive(false);
            Application.Quit();
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpSpeed);
    }

    void SetCountText()
    {
        countText.text = "Cookie Count: " + count.ToString();
    }
}
